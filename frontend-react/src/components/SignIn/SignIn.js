import React, {useEffect, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Checkbox from '@material-ui/core/Checkbox';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {routes, UiState} from "../utils";
import {NavLink, useHistory} from "react-router-dom";
import axios from "axios";
import {api} from "../../lib/api";
import style from "../App/App.module.scss";
import {CircularProgress} from "@material-ui/core";
import {saveJwt} from "../../lib/storage";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(1),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

function SignIn() {
    const classes = useStyles();
    const history = useHistory();

    const [state, setState] = useState(UiState.Ready)
    const [error, setError] = useState(null)
    const [rememberMeChecked, setRememberMeChecked] = useState(true)

    const dispatchError = (error) => {
        setError(error.toString())//todo
        setState(UiState.Error)
        console.log(error)
    }

    const dispatchLoading = () => {
        setState(UiState.Loading)
    }

    const dispatchReady = () => {
        setState(UiState.Ready)
    }

    const navigateMain = () => {
        history.push(routes.main)
    }

    const navigateSignUp = () => {
        history.push(routes.signup)
    }

    const saveLogin = (jwt) => {
        saveJwt(jwt, rememberMeChecked)
    }

    const handleSubmit = async (event) => {
        event.preventDefault();
        try {
            dispatchLoading()
            const data = new FormData(event.target);
            const loginForm = {
                email: data.get('email'),
                password: data.get('password')
            }
            const {email, password} = loginForm
            console.log(loginForm);
            const base64Authorization = btoa(`${email}:${password}`);
            let response = await axios.post(api.login, null, {
                headers: {
                    'Authorization': 'Basic ' + base64Authorization,
                }
            })
            saveLogin(response.data)
            console.log(response)
            dispatchReady()
            navigateMain()
        } catch (e) {
            console.log(e)
            let error
            if (e?.response?.data) {
                let status = e?.response.status
                if (status === 403) {
                    error = "Неверно введены логин или пароль"
                }
            } else {
                error = "Неопознанная ошибка"
            }
            // dispatchError(error)
            setTimeout(navigateSignUp, 500)
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            {state !== UiState.Ready &&
            <div className={style.modal}>
                {
                    state === UiState.Loading && <CircularProgress color={"secondary"}/>
                }
                {
                    state === UiState.Error && <div>
                        <div className={style.close} onClick={() => dispatchReady()}>
                        </div>
                        <div className={style.error}>
                            {error}
                        </div>
                    </div>
                }
            </div>
            }
            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Вход
                </Typography>
                <form className={classes.form} onSubmit={handleSubmit}>
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        id="email"
                        label="Логин"
                        name="email"
                        autoComplete="email"
                        autoFocus
                    />
                    <TextField
                        variant="outlined"
                        margin="normal"
                        required
                        fullWidth
                        name="password"
                        label="Пароль"
                        type="password"
                        id="password"
                        autoComplete="current-password"
                    />
                    <FormControlLabel
                        control={<Checkbox value="remember" color="primary" defaultChecked={true}/>}
                        onChange={(e, checked)=> {
                            setRememberMeChecked(checked)
                        }}
                        label="Запомнить меня"
                    />
                    <Button
                        type="submit"
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Войти
                    </Button>
                    <Grid container>
                       {/* <Grid item xs>
                            <Link variant="body2">
                                Забыли пароль?
                            </Link>
                        </Grid>*/}
                        <Grid item>
                            <NavLink to={routes.signup} variant="body2">
                                {"Нет аккаунта? Зарегистрируйтесь"}
                            </NavLink>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}

export default SignIn;