import React, {createRef, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import CssBaseline from '@material-ui/core/CssBaseline';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {NavLink, useHistory} from "react-router-dom";
import {routes, UiState} from "../utils";
import axios from "axios";
import {api} from "../../lib/api";
import style from "../App/App.module.scss";
import {CircularProgress} from "@material-ui/core";

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function SignUp() {
    const classes = useStyles();
    const history = useHistory();

    const [state, setState] = useState(UiState.Ready)
    const [error, setError] = useState(null)
    const formRef = createRef()


    const dispatchError = (error) => {
        setError(error.toString())//todo
        setState(UiState.Error)
        console.log(error)
    }

    const dispatchLoading = () => {
        setState(UiState.Loading)
    }

    const dispatchReady = () => {
        setState(UiState.Ready)
    }

    const handleError = (error) => {
        console.log(error?.response)
        if (error?.response?.status === 500) {
            dispatchError("Аккаунт с таким логином уже зарегистрирован! Попробовать ещё раз?")
            // setError("Аккаунт с таким email уже зарегистрирован! Попробовать ещё раз?")
        } else {
            dispatchError("Неопознанная ошибка! Попробовать ещё раз?")
            // setError("Неопознанная ошибка! Попробовать ещё раз?")
        }
    }

    const navigateSignIn = () => {
        history.push(routes.signin)
    }

    const onSubmit = async () => {//todo: add checks for email
        if (!formRef.current.reportValidity()) {
            return
        }
        try {
            dispatchLoading()
            const data = new FormData(formRef.current);
            const userForm = {
                firstName: data.get('name'),
                secondName: data.get('last_name'),
                email: data.get('email'),
                password: data.get('password')
            }

            console.log(userForm);
            await axios.post(api.register, userForm)
            dispatchReady()
            navigateSignIn(userForm)
        } catch (error) {
            handleError(error)
        }
    }

    return (
        <Container component="main" maxWidth="xs">
            <CssBaseline/>
            {state !== UiState.Ready &&
            <div className={style.modal}>
                {
                    state === UiState.Loading && <CircularProgress color={"secondary"}/>
                }
                {
                    state === UiState.Error && <div>
                        <div className={style.close} onClick={() => dispatchReady()}>
                        </div>
                        <div className={style.error}>
                            {error}
                        </div>
                    </div>
                }
            </div>
            }

            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Регистрация
                </Typography>
                <form className={classes.form} id={"signup-form"} ref={formRef}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="name"
                                variant="outlined"
                                required
                                fullWidth
                                id="name"
                                label="Имя"
                                autoFocus
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                id="last_name"
                                label="Фамилия"
                                name="last_name"
                                autoComplete="lname"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                inputProps={{minLength: 9}}
                                id="email"
                                label="Логин"
                                name="email"
                                autoComplete="email"
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                required
                                fullWidth
                                name="password"
                                label="Пароль"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                            />
                        </Grid>
                    </Grid>
                    <Button
                        type="button"
                        onClick={onSubmit}
                        fullWidth
                        variant="contained"
                        color="primary"
                        className={classes.submit}
                    >
                        Зарегистрироваться
                    </Button>
                    <Grid container justify="flex-end">
                        <Grid item>
                            <NavLink to={routes.signin} variant="body2">
                                Уже есть аккаунт? Войти
                            </NavLink>
                        </Grid>
                    </Grid>
                </form>
            </div>
        </Container>
    );
}