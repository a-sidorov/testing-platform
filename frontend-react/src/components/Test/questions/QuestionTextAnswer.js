import React from "react";
import {Grid, Icon, Paper, TextField, Typography} from "@material-ui/core";

export function QuestionTextAnswer(props) {
    const {id, index, questionText, updateQuestionSubmit, disabled, correctAnswer, correct} = props

    let handleTextChange = (textAnswer) => {
        updateQuestionSubmit?.(textAnswer, id)
    };

    let color
    if (correct === undefined) {
        color = undefined
    } else {
        color = correct ? "green" : "red"
    }

    return <Grid key={id}>
        <Paper style={{marginTop: 20}}>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginLeft: '6px',
                paddingTop: '15px',
                paddingBottom: '15px'
            }}>
                {color && <Icon style={{background:color}}></Icon>}
                <Typography variant="subtitle1"
                            style={{marginLeft: '10px'}}>{index + 1}. {questionText}</Typography>
                <div style={{width: 440}}>
                    <TextField
                        style={{
                            marginLeft: "12px",
                        }}
                        fullWidth={true}
                        inputProps={{maxLength: 100}}
                        multiline
                        onChange={(e) => {
                            handleTextChange(e.target.value, id)
                        }}
                        disabled={disabled}
                        defaultValue={correctAnswer}
                    />
                </div>
            </div>
        </Paper>
    </Grid>
}