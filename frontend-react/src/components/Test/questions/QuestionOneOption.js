import React, {useState} from "react";
import {Grid, Icon, Paper, Typography} from "@material-ui/core";
import RadioGroup from '@material-ui/core/RadioGroup';
import FormControlLabel from '@material-ui/core/FormControlLabel';
import Radio from '@material-ui/core/Radio';

export function QuestionOneOption(props) {
    const [value, setValue] = useState(null);
    const {id, index, questionText, options, updateQuestionSubmit, disabled, correct} = props

    let handleRadioChange = (option_id_str) => {
        let option_id = parseInt(option_id_str)
        setValue(option_id)
        updateQuestionSubmit?.(option_id, id)
    };
    let color
    if (correct === undefined) {
        color = undefined
    } else {
        color = correct ? "green" : "red"
    }

    return <Grid key={id}>
        <Paper style={{marginTop: 20}}>
            <div style={{
                display: 'flex',
                flexDirection: 'column',
                alignItems: 'flex-start',
                marginLeft: '6px',
                paddingTop: '15px',
                paddingBottom: '15px'
            }}>
                {color && <Icon style={{background:color}}></Icon>}
                <Typography variant="subtitle1"
                            style={{marginLeft: '10px'}}>{index + 1}. {questionText}</Typography>

                <RadioGroup aria-label="quiz" name="quiz" value={value} onChange={(e) => {
                    handleRadioChange(e.target.value, id)
                }}>

                    {options.map((option) => (
                        <div key={option.id}>
                            <div style={{display: 'flex', marginLeft: '7px'}}>
                                <FormControlLabel value={option.id} control={
                                    disabled ?
                                        <Radio disabled={true} checked={option.correct}/> :
                                        <Radio/>
                                }
                                                  label={option.optionText}/>
                            </div>
                        </div>
                    ))}
                </RadioGroup>
            </div>
        </Paper>
    </Grid>
}