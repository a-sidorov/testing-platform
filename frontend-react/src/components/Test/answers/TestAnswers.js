import React, {useEffect, useState} from "react";
import {localStorageKeys, routes, UiState} from "../../utils";
import axios from "axios";
import {api} from "../../../lib/api";
import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import {makeStyles} from "@material-ui/core/styles";
import style from "../../App/App.module.scss";
import {AppBar, CircularProgress} from "@material-ui/core";
import Container from "@material-ui/core/Container";
import Toolbar from "@material-ui/core/Toolbar";
import HomeIcon from "@material-ui/icons/Home";
import IconButton from "@material-ui/core/IconButton";
import {useHistory} from "react-router-dom";
import {getJwt} from "../../../lib/storage";

const useStyles = makeStyles((theme) => ({
    toolbar: {
        minHeight: 70,
        textAlign: "center",
        alignItems: 'flex-start',
        paddingTop: theme.spacing(1),
    },
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    }
}));

export function TestAnswers() {
    const history = useHistory()
    const classes = useStyles();

    const [answers, setAnswers] = useState([])
    const [uiState, setUiState] = useState(UiState.Ready)
    const [error, setError] = useState(null)

    const jwtToken = getJwt()
    useEffect(fetchAnswers, [])


    const dispatchLoading = () => {
        setUiState(UiState.Loading)
    }

    const dispatchError = (error) => {
        if (error?.response) {
            console.log(error?.response)
        }
        setError(error.toString())//todo
        setUiState(UiState.Error)
        console.log(error)
    }

    const dispatchReady = () => {
        setUiState(UiState.Ready)
    }

    async function fetchAnswers() {
        try {
            dispatchLoading()
            const testAnswers = await axios.get(api.userTestAnswers, {
                headers: {
                    Authorization: 'Bearer ' + jwtToken
                }
            }).then((r) => {
                console.log(r)
                return r.data
            })
            setAnswers(testAnswers)
            dispatchReady()
        } catch (e) {
            dispatchError(e)
        }
    }

    function navigateHome() {
        history.push(routes.main)
    }

    return (
        <div>
            <AppBar position="static" style={{backgroundColor: 'white'}} elevation={2}>
                <Toolbar className={classes.toolbar}>
                    <IconButton onClick={navigateHome}>
                        <HomeIcon/>
                    </IconButton>
                    <Typography variant="h6" noWrap style={{marginTop: '8.5px', color: 'black'}}>
                        Результаты тестирований
                    </Typography>
                </Toolbar>
            </AppBar>
            {uiState !== UiState.Ready &&
            <div className={style.modal}>
                {
                    uiState === UiState.Loading && <CircularProgress color={"secondary"}/>
                }
                {
                    uiState === UiState.Error && <div>
                        <div className={style.close} onClick={() => dispatchReady()}>
                        </div>
                        <div className={style.error}>
                            {error}
                        </div>
                    </div>
                }
            </div>
            }
            <Container className={classes.cardGrid} maxWidth="md">
                {answers && answers.length !== 0 ? <Grid container spacing={4}>
                        {answers?.map((answer) => {
                            return <Grid item xs={12} sm={6} md={4} style={{marginBottom: "20px"}}>
                                <Card className={classes.card} variant={"outlined"}>
                                    <CardContent className={classes.cardContent}>
                                        <Typography gutterBottom variant="h5" component="h2">
                                            {answer.name}
                                        </Typography>
                                        <Typography>
                                            {answer.description}
                                        </Typography>
                                        <Typography>
                                            Результат: {answer.result}
                                        </Typography>
                                    </CardContent>
                                </Card>
                            </Grid>
                        })}
                    </Grid> :
                    <Typography gutterBottom variant="h5" component="h2" style={{marginLeft: "25%"}}>
                        Вы ещё не ответили ни на один тест :(
                    </Typography>
                }
            </Container>
        </div>
    )
}