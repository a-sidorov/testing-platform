import React, {useEffect} from 'react'
import {Checkbox, FormControl, Grid, InputLabel, MenuItem, Paper, Select, Typography} from '@material-ui/core';
import {DragDropContext, Draggable, Droppable} from "react-beautiful-dnd";
import TextField from '@material-ui/core/TextField';
import Accordion from '@material-ui/core/Accordion';
import AccordionSummary from '@material-ui/core/AccordionSummary';
import AccordionDetails from '@material-ui/core/AccordionDetails';
import Button from '@material-ui/core/Button';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';
import Radio from '@material-ui/core/Radio';

import FormControlLabel from '@material-ui/core/FormControlLabel';
import AccordionActions from '@material-ui/core/AccordionActions';
import Divider from '@material-ui/core/Divider';
import VisibilityIcon from '@material-ui/icons/Visibility';
import DeleteOutlineIcon from '@material-ui/icons/DeleteOutline';
import FilterNoneIcon from '@material-ui/icons/FilterNone';
import AddCircleIcon from '@material-ui/icons/AddCircle';
import DragIndicatorIcon from '@material-ui/icons/DragIndicator';
import CircularProgress from '@material-ui/core/CircularProgress';
import {questionTypes} from "../types";
import {DateRangePicker} from "materialui-daterange-picker";
import {getFormattedDate} from "../utils";

export function TestEdit(props) {
    const {updateFormData, formData: formDataProps} = props
    const [questions, setQuestions] = React.useState([]);
    const [formData, setFormData] = React.useState({name: "", description: ""});
    const [loadingFormData, setLoadingFormData] = React.useState(false);
    const [openDate, setOpenDate] = React.useState(false);
    const [dateRange, setDateRange] = React.useState({startDate: null, endDate: null});
    const [dateText, setDateText] = React.useState("Выберите дату")

    const toggleDatePicker = () => setOpenDate(!openDate);

    useEffect(() => {
        let text
        if (dateRange.startDate && dateRange.endDate) {
            text = getFormattedDate(dateRange.startDate) + " -> " + getFormattedDate(dateRange.endDate)
        } else {
            text = "Выберите дату"
        }
        setDateText(text)
    }, [dateRange])

    useEffect(() => {
        console.log({formDataProps: formDataProps})
        if (formDataProps !== undefined) {
            const {id, name, description, questions, startDate, endDate} = formDataProps
            setFormData({id: id, name: name, description: description})
            setDateRange({startDate: startDate, endDate: endDate})
            if (questions !== undefined && !questions.isEmpty) {
                questions.forEach((question) => {
                    question.open = false
                })
                setQuestions(questions)
            } else {
                resetQuestions()
            }
        } else {
            resetQuestions()
        }
    }, [props.formData])

    useEffect(() => {
        let data = getFormData()
        updateFormData?.(data)
    }, [formData, questions, dateRange])

    function resetQuestions() {
        setQuestions([{
            questionText: "Question",
            options: [{optionText: "Option 1", correct: false}],
            open: false,
            type: questionTypes.oneOption
        }]);
    }

    function getFormData() {
        let mappedQuestions = []
        questions?.forEach((question) => {
            let submitQuestion = {type: question.type}
            submitQuestion.questionText = question.questionText

            if (question.type !== questionTypes.textAnswer) {
                submitQuestion.options = question.options
            } else {
                submitQuestion.correctAnswer = question.correctAnswer
            }
            mappedQuestions.push(submitQuestion)
        })
        var data = {
            name: formData.name,
            description: formData.description,
            questions: mappedQuestions,
            startDate: getFormattedDate(dateRange.startDate),
            endDate: getFormattedDate(dateRange.endDate),
        }
        if (formData.id !== undefined) {
            data.id = formData.id
        }
        return data
    }

    function addMoreQuestionField() {
        closeAllQuestions();

        setQuestions(questions => [...questions, {
            questionText: "Question",
            options: [{optionText: "Option 1", correct: false}],
            open: true,
            type: questionTypes.oneOption
        }]);
    }

    function copyQuestion(i) {
        let qs = [...questions];
        closeAllQuestions();
        const myNewOptions = [];
        if (qs[i].type !== questionTypes.textAnswer) {
            qs[i].options.forEach(option => {
                var newOption = {
                    optionText: option.optionText,
                    correct: option.correct
                }
                myNewOptions.push(newOption)
            });
        }

        var newQuestion = {
            type: qs[i].type,
            questionText: qs[i].questionText,
            options: myNewOptions,
            open: true,
            correctAnswer: qs[i].correctAnswer
        }
        setQuestions(questions => [...questions, newQuestion]);
    }

    function deleteQuestion(i) {
        let qs = [...questions];
        if (questions.length > 1) {
            qs.splice(i, 1);
        }
        setQuestions(qs)
    }

    function handleOptionValue(text, i, j) {
        var optionsOfQuestion = [...questions];
        optionsOfQuestion[i].options[j].optionText = text;
        setQuestions(optionsOfQuestion);
    }

    function handleQuestionValue(text, i) {
        var optionsOfQuestion = [...questions];
        optionsOfQuestion[i].questionText = text;
        setQuestions(optionsOfQuestion);
    }

    function onDragEnd(result) {
        if (!result.destination) {
            return;
        }
        var itemgg = [...questions];

        const itemF = reorder(
            itemgg,
            result.source.index,
            result.destination.index
        );

        setQuestions(itemF);
    }

    const reorder = (list, startIndex, endIndex) => {
        const result = Array.from(list);
        const [removed] = result.splice(startIndex, 1);
        result.splice(endIndex, 0, removed);
        return result;
    };

    function showAsQuestion(i) {
        let qs = [...questions];
        qs[i].open = false;
        setQuestions(qs);
    }

    function addOption(i) {
        var optionsOfQuestion = [...questions];
        if (optionsOfQuestion[i].options.length < 5) {
            optionsOfQuestion[i].options.push({
                optionText: "Option " + (optionsOfQuestion[i].options.length + 1),
                correct: false
            })
            setQuestions(optionsOfQuestion)
        }
    }

    function removeOption(i, j) {
        var optionsOfQuestion = [...questions];
        if (optionsOfQuestion[i].options.length > 1) {
            optionsOfQuestion[i].options.splice(j, 1);
            setQuestions(optionsOfQuestion)
        }
    }

    function closeAllQuestions() {
        questions.forEach((question) => {
            question.open = false
        })
    }

    function handleExpand(i) {
        questions.forEach((question, j) => {
            question.open = i === j;
        })
        setQuestions([...questions]);
    }

    const handleQuestionTypeChange = (event, question) => {
        console.log(event.target.value)
        question.type = event.target.value
        question.options = [{
            optionText: "Option " + 1,
            correct: false
        }]
        setQuestions([...questions]);
    };

    const handleCorrectAnswerChange = (event, question) => {
        if (question.type === questionTypes.textAnswer) {
            question.correctAnswer = event.target.value
        } else if (question.type === questionTypes.oneOption) {
            question.options.forEach((option) => {
                option.correct = option.optionText === event
            })
        } else if (question.type === questionTypes.manyOptions) {
            let option = question.options.find((option => option.optionText === event))
            option.correct = !option.correct
        }
        setQuestions([...questions]);
    }

    function questionsUI() {
        return questions?.map((question, i) => (
            <Draggable key={i} draggableId={i + 'id'} index={i}>
                {(provided) => (
                    <div
                        ref={provided.innerRef}
                        {...provided.draggableProps}
                        {...provided.dragHandleProps}>
                        <div style={{marginBottom: "15px"}}>
                            <div style={{width: '100%', marginBottom: '-7px'}}>
                                <DragIndicatorIcon style={{transform: "rotate(-90deg)", color: '#DAE0E2'}}
                                                   fontSize="small"/>
                            </div>
                            <Accordion
                                onChange={() => {
                                    handleExpand(i)
                                }}
                                expanded={questions[i].open}>
                                <AccordionSummary
                                    aria-controls="panel1a-content"
                                    id="panel1a-header"
                                    elevation={1} style={{width: '100%'}}>
                                    {!questions[i].open ? (
                                        <div style={{
                                            display: 'flex',
                                            flexDirection: 'column',
                                            alignItems: 'flex-start',
                                            marginLeft: '3px',
                                            paddingTop: '15px',
                                            paddingBottom: '15px'
                                        }}>

                                            <Typography variant="subtitle1"
                                                        style={{marginLeft: '0px'}}>{i + 1}. {question.questionText}</Typography>

                                            {
                                                question.type === questionTypes.textAnswer &&
                                                <TextField placeholder='Ответ' value={question.correctAnswer}
                                                           disabled={true}/>
                                            }
                                            {question.type !== questionTypes.textAnswer && question.options.map((option, j) => (
                                                <div key={j}>
                                                    <div style={{display: 'flex'}}>
                                                        {
                                                            question.type === questionTypes.oneOption ?
                                                                <FormControlLabel disabled control={
                                                                    <Radio
                                                                        style={{marginRight: '3px',}}
                                                                        checked={option.correct}
                                                                    />} label={
                                                                    <Typography style={{color: '#555555'}}>
                                                                        {question.options[j].optionText}
                                                                    </Typography>
                                                                }/> : null
                                                        }
                                                        {
                                                            question.type === questionTypes.manyOptions ?
                                                                <FormControlLabel disabled control={<Checkbox
                                                                    style={{marginRight: '3px'}}
                                                                    checked={option.correct}
                                                                />} label={
                                                                    <Typography style={{color: '#555555'}}>
                                                                        {question.options[j].optionText}
                                                                    </Typography>
                                                                }/> : null
                                                        }
                                                    </div>
                                                </div>
                                            ))}
                                        </div>
                                    ) : ""}
                                </AccordionSummary>

                                <AccordionDetails>
                                    <div style={{
                                        display: 'flex',
                                        flexDirection: 'column',
                                        alignItems: 'flex-start',
                                        marginLeft: '15px',
                                        marginTop: '-15px'
                                    }}>
                                        <div style={{
                                            display: 'flex',
                                            width: '100%',
                                            justifyContent: 'space-between'
                                        }}>
                                            <Typography style={{marginTop: '20px'}}>{i + 1}.</Typography>
                                            <TextField
                                                fullWidth={true}
                                                placeholder="Question Text"
                                                style={{marginBottom: '18px'}}
                                                rows={2}
                                                rowsMax={20}
                                                multiline={true}
                                                value={question.questionText}
                                                variant="filled"
                                                onChange={(e) => {
                                                    handleQuestionValue(e.target.value, i)
                                                }}
                                            />
                                        </div>
                                        <div style={{width: '100%'}}>
                                            {question.type === questionTypes.textAnswer ?
                                                <TextField
                                                    style={{marginLeft: 15, marginTop: 10}}
                                                    value={question.correctAnswer}
                                                    placeholder='Введите правильный ответ'
                                                    onChange={(event) =>
                                                        handleCorrectAnswerChange(event, question)
                                                    }
                                                /> : null
                                            }
                                            {question.type !== questionTypes.textAnswer &&
                                            question.options.map((option, j) => (
                                                <div key={j} style={{
                                                    display: 'flex',
                                                    flexDirection: 'row',
                                                    marginLeft: '-12.5px',
                                                    justifyContent: 'space-between',
                                                    paddingTop: '5px',
                                                    paddingBottom: '5px'
                                                }}>
                                                    {question.type === questionTypes.oneOption ?
                                                        <Radio checked={option.correct}
                                                               onClick={() =>
                                                                   handleCorrectAnswerChange(
                                                                       option.optionText,
                                                                       question
                                                                   )
                                                               }/> : null
                                                    }

                                                    {question.type === questionTypes.manyOptions ?
                                                        <Checkbox checked={option.correct}
                                                                  onClick={() =>
                                                                      handleCorrectAnswerChange(
                                                                          option.optionText,
                                                                          question
                                                                      )
                                                                  }
                                                        /> : null
                                                    }

                                                    <TextField
                                                        fullWidth={true}
                                                        placeholder="Option text"
                                                        style={{marginTop: '5px'}}
                                                        value={question.options[j].optionText}
                                                        onChange={(e) => {
                                                            handleOptionValue(e.target.value, i, j)
                                                        }}
                                                    />

                                                    <IconButton aria-label="delete" onClick={() => {
                                                        removeOption(i, j)
                                                    }}>
                                                        <CloseIcon/>
                                                    </IconButton>
                                                </div>
                                            ))}
                                        </div>

                                        {question.type !== questionTypes.textAnswer &&
                                        question.options.length < 5 ? (
                                            <div>
                                                {question.type === questionTypes.oneOption ?
                                                    <FormControlLabel disabled control={<Radio/>} label={
                                                        <Button size="small" onClick={() => {
                                                            addOption(i)
                                                        }} style={{textTransform: 'none', marginLeft: "-5px"}}>
                                                            Add Option
                                                        </Button>
                                                    }/> : null
                                                }

                                                {question.type === questionTypes.manyOptions ?
                                                    <FormControlLabel disabled control={<Checkbox/>} label={
                                                        <Button size="small" onClick={() => {
                                                            addOption(i)
                                                        }} style={{textTransform: 'none', marginLeft: "-5px"}}>
                                                            Add Option
                                                        </Button>
                                                    }/> : null
                                                }
                                            </div>
                                        ) : ""}

                                        {question.type !== questionTypes.textAnswer ?
                                            <Typography variant="body2" style={{color: 'grey', paddingTop: 20}}>
                                                Максимальное количество опций: 5
                                            </Typography> : null}
                                    </div>
                                </AccordionDetails>

                                <Divider/>

                                <AccordionActions>
                                    <FormControl style={{
                                        margin: 12,
                                        minWidth: 120,
                                    }}>
                                        <InputLabel id="demo-simple-select-label">Тип</InputLabel>
                                        <Select
                                            labelId="demo-simple-select-label"
                                            id="demo-simple-select"
                                            value={question.type}
                                            onChange={(event) => handleQuestionTypeChange(event, question)}
                                        >
                                            <MenuItem value={questionTypes.oneOption}>Один выбор</MenuItem>
                                            <MenuItem value={questionTypes.manyOptions}>Выбор нескольких</MenuItem>
                                            <MenuItem value={questionTypes.textAnswer}>Текстовый ответ</MenuItem>
                                        </Select>
                                    </FormControl>
                                    <IconButton aria-label="View" onClick={() => {
                                        showAsQuestion(i)
                                    }}>
                                        <VisibilityIcon/>
                                    </IconButton>

                                    <IconButton aria-label="Copy" onClick={() => {
                                        copyQuestion(i)
                                    }}>
                                        <FilterNoneIcon/>
                                    </IconButton>
                                    <Divider orientation="vertical" flexItem/>

                                    <IconButton aria-label="delete"
                                                onClick={() => {
                                                    deleteQuestion(i)
                                                }}>
                                        <DeleteOutlineIcon/>
                                    </IconButton>
                                </AccordionActions>
                            </Accordion>
                        </div>
                    </div>
                )}
            </Draggable>
        ))
    }

    return (
        <div style={{marginTop: '15px', marginBottom: '7px', paddingBottom: "30px"}}>
            <Grid
                container
                direction="column"
                justify="center"
                alignItems="center">
                {loadingFormData ? (<CircularProgress/>) : ""}

                <Grid item xs={12} sm={5} style={{width: '100%'}}>
                    <Grid style={{borderTop: '10px solid teal', borderRadius: 10}}>
                        <Paper elevation={2} style={{width: '100%'}}>
                            <div style={{
                                display: 'flex',
                                flexDirection: 'column',
                                alignItems: 'flex-start',
                                marginLeft: '15px',
                                paddingTop: '20px',
                                paddingBottom: '20px'
                            }}>
                                <TextField
                                    label='Заголовок'
                                    multiline={true}
                                    value={formData.name}
                                    inputProps={{maxLength: 50}}
                                    onChange={(event) => {
                                        setFormData({id: formData.id, name: event.target.value, description: formData.description})
                                    }}
                                    style={{
                                        width: "340px",
                                        fontFamily: 'sans-serif Roboto',
                                        marginBottom: "15px",
                                        marginLeft: "20px"
                                    }}/>
                                <TextField
                                    onChange={(event) => {
                                        setFormData({id: formData.id, name: formData.name, description: event.target.value})
                                    }}
                                    style={{marginLeft: "20px", width: "350px"}}
                                    multiline={true}
                                    inputProps={{maxLength: 300}}
                                    label='Описание'
                                    value={formData.description}
                                />
                                <TextField
                                    style={{marginLeft: "20px", marginTop: "20px", width: "350px"}}
                                    multiline={true}
                                    value={dateText}
                                    onClick={toggleDatePicker}
                                    label='Время доступности теста'
                                    disabled={true}
                                />
                                <DateRangePicker
                                    style={{marginTop: "20px"}}
                                    open={openDate}
                                    toggle={toggleDatePicker}
                                    onChange={(range) => setDateRange(range)}
                                />
                            </div>
                        </Paper>
                    </Grid>

                    <Grid style={{paddingTop: '10px'}}>
                        <DragDropContext onDragEnd={onDragEnd}>
                            <Droppable droppableId="droppable">
                                {(provided) => (
                                    <div {...provided.droppableProps}
                                         ref={provided.innerRef}>
                                        {questionsUI()}
                                        {provided.placeholder}
                                    </div>
                                )}
                            </Droppable>
                        </DragDropContext>
                        <Button
                            fullWidth={true}
                            variant="contained"
                            onClick={addMoreQuestionField}
                            endIcon={<AddCircleIcon/>}
                            style={{margin: '5px'}}>Добавить вопрос</Button>
                    </Grid>
                </Grid>
            </Grid>
        </div>
    );
}