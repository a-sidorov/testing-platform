import React, {useEffect, useState} from "react";
import {TestShow} from "./TestShow";
import {useHistory, useLocation} from "react-router-dom";
import {questionTypes} from "../types";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import HomeIcon from '@material-ui/icons/Home';
import {AppBar, CircularProgress, Tab, Tabs, Typography} from "@material-ui/core";
import EditIcon from "@material-ui/icons/Edit";
import DoneIcon from '@material-ui/icons/Done';
import {makeStyles} from "@material-ui/core/styles";
import {routes, UiState} from "../../utils";
import {TestEdit} from "../edit/TestEdit";
import LinkIcon from '@material-ui/icons/Link';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogContentText from '@material-ui/core/DialogContentText';
import DialogTitle from '@material-ui/core/DialogTitle';
import FileCopyIcon from '@material-ui/icons/FileCopy';
import DeleteIcon from '@material-ui/icons/Delete';
import axios from "axios";
import {api} from "../../../lib/api";
import style from "../../App/App.module.scss";
import {ResponsesTab} from "./ResponsesTab";
import {getJwt} from "../../../lib/storage";

const useStyles = makeStyles((theme) => ({
    toolbar: {
        minHeight: 70,
        textAlign: "center",
        alignItems: 'flex-start',
        paddingTop: theme.spacing(1),
    },
    title: {
        flexGrow: 1,
        alignSelf: 'flex-end',
        justifySelf: 'center'
    },
}));

const tabs = {
    test: 1,
    responses: 2
}

export function TestView(props) {
    const classes = useStyles();
    const jwtToken = getJwt()
    const history = useHistory()
    const location = useLocation()
    const urlParams = new URLSearchParams(location.search)
    let edit = false
    let id

    if (urlParams.has("id")) {
        id = parseInt(urlParams.get("id"))
    }
    if (urlParams.has("edit")) {
        edit = urlParams.get("edit") === "true"
    }
    const [editState, setEditState] = useState(edit)
    const [formData, setFormData] = useState({})
    const [editedFormData, setEditedFormData] = useState({})
    const [state, setState] = useState(UiState.Ready)
    const [error, setError] = useState(null)
    const [open, setOpen] = useState(false)
    const [tab, setTab] = useState(tabs.test)
    const [responses, setResponses] = useState()

    const dispatchError = (error) => {
        if (error?.response?.data) {
            console.log(error.response.data)
        }
        setError(error.toString())//todo
        setState(UiState.Error)
        console.log(error)
    }

    const dispatchLoading = () => {
        setState(UiState.Loading)
    }

    const dispatchReady = () => {
        setState(UiState.Ready)
    }

    const fetchTest = async () => {
        if (id === undefined) {
            return
        }
        try {
            dispatchLoading()
            let promise = axios.post(api.getTestById, id, {
                headers: {
                    Authorization: 'Bearer ' + jwtToken,
                    'content-type': 'application/json'
                }
            })

            const formData = await promise.then((r) => r.data)
            console.log(formData)
            dispatchReady()
            return formData
        } catch (e) {
            console.log(e?.response?.data)
            dispatchError(e)
        }
    }

    async function removeTest() {
        if (id === undefined) {
            return
        }
        try {
            dispatchLoading()
            let res = await axios.post(api.removeTest, id, {
                headers: {
                    Authorization: 'Bearer ' + jwtToken,
                    'content-type': 'application/json'
                }
            })
            console.log(res)
            dispatchReady()
            setTimeout(navigateHome, 100)
        } catch (e) {
            console.log(e?.response?.data)
            dispatchError(e)
        }
    }

    useEffect(async () => {
        const data = await fetchTest()
        if (data !== undefined) {
            setFormData(data)
        }
    }, [])

    useEffect(() => {
        console.log(formData)
    }, [formData])


    function navigateHome() {
        history.push(routes.main)
    }

    useEffect(() => {
        if (tab === tabs.responses && !responses) {
            fetchResponses()
        }
    }, [tab])

    async function fetchResponses() {
        if (id === undefined) {
            return
        }
        try {
            dispatchLoading()
            console.log({jwtToken, id})
            const testResponses = await axios.post(api.testResponses, id, {
                headers: {
                    Authorization: 'Bearer ' + jwtToken,
                    'content-type': 'application/json'
                }
            }).then((r) => {
                console.log(r)
                return r.data
            })
            setResponses(testResponses)
            dispatchReady()
        } catch (e) {
            dispatchError(e)
        }
    }

    async function createCall(formData) {
        let promise = axios.post(api.createTest, formData, {
            headers: {
                Authorization: 'Bearer ' + jwtToken
            }
        })
        return await promise.then((res) => res.data)
    }

    async function editCall(formData) {
        let promise = axios.post(api.editTest, formData, {
            headers: {
                Authorization: 'Bearer ' + jwtToken
            }
        })
        return await promise.then((res) => res.data)
    }

    function isFormCorrect(formData) {//todo: add start end dates
        const {name, description, questions, startDate, endDate} = formData
        if (name === undefined || name.toString().length === 0) {
            alert("Заголовок не должен быть пустым")
            return false
        }
        if (description === undefined || description.toString().length === 0) {
            alert("Описание не должен быть пустым")
            return false
        }

        if (startDate === undefined || startDate === null) {
            alert("Укажите период доступности теста")
            return false
        }

        if (questions !== undefined && questions.length !== 0) {
            for (let i = 0; i < questions.length; i++) {
                const {questionText, type, correctAnswer, options} = questions[i]
                if (questionText === undefined || questionText.toString().length === 0) {
                    alert(`Вопрос № ${i + 1} не должен быть пустым`)
                    return false
                }
                if (type === questionTypes.textAnswer) {
                    if (correctAnswer === undefined || correctAnswer.toString().length === 0) {
                        alert(`Укажите правильный ответ в вопросе № ${i + 1}`)
                        return false
                    }
                } else {
                    const correctAnswer = options.find((option) => {
                        return option.correct
                    })
                    if (correctAnswer === undefined) {
                        alert(`Укажите правильный ответ в вопросе № ${i + 1}`)
                        return false
                    }
                }
            }
        }

        return true
    }

    async function saveTest() {
        console.log(editedFormData)
        if (!isFormCorrect(editedFormData)) {
            return
        }
        try {
            dispatchLoading()
            let data
            if (formData.id !== undefined) {
                data = await editCall(editedFormData)
            } else {
                data = await createCall(editedFormData)
            }
            dispatchReady()
            console.log(data)
            editedFormData.id = data.id

            setFormData(editedFormData)
            setEditState(false)
        } catch (e) {
            dispatchError(e)
        }

    }

    const handleChange = (event, newValue) => {
        setTab(newValue);
    };

    return (
        <div>
            <AppBar position="static" style={{backgroundColor: 'white'}} elevation={2}>
                <Toolbar className={classes.toolbar}>
                    <IconButton onClick={navigateHome}>
                        <HomeIcon/>
                    </IconButton>
                    <Typography variant="h6" noWrap style={{marginTop: '8.5px', color: 'black'}}>
                        {formData ? formData.name : "Untitled"}
                    </Typography>
                    {tab === tabs.test && <div>
                        {editState ?
                            <IconButton onClick={() => saveTest()}>
                                <DoneIcon/>
                            </IconButton>
                            :
                            <IconButton onClick={() => setEditState(true)}>
                                <EditIcon/>
                            </IconButton>
                        }
                        {!editState && formData &&
                        <IconButton onClick={() => setOpen(true)}>
                            <LinkIcon/>
                        </IconButton>
                        }
                        <IconButton onClick={() => removeTest()}>
                            <DeleteIcon/>
                        </IconButton>
                    </div>
                    }

                </Toolbar>
            </AppBar>
            {!editState && <Tabs
                className={classes.title}
                value={tab}
                onChange={handleChange}
                indicatorColor="primary"
                textColor="primary"
                centered
            >
                <Tab label="Тест" value={tabs.test}/>
                <Tab label="Ответы" value={tabs.responses}/>
            </Tabs>}

            {open && formData &&
            <LinkFormDialog
                id={formData?.id}
                setOpen={setOpen}
            />
            }
            {state !== UiState.Ready &&
            <div className={style.modal}>
                {
                    state === UiState.Loading && <CircularProgress color={"secondary"}/>
                }
                {
                    state === UiState.Error && <div>
                        <div className={style.close} onClick={() => dispatchReady()}>
                        </div>
                        <div className={style.error}>
                            {error}
                        </div>
                    </div>
                }
            </div>
            }
            {
                tab === tabs.test ?
                    (editState ?
                            <TestEdit
                                formData={formData}
                                updateFormData={(data) => setEditedFormData(data)}
                            />
                            :
                            <TestShow
                                formData={formData}
                            />
                    ) :
                    <ResponsesTab
                        responses={responses}
                    />
            }
        </div>
    )
}

function LinkFormDialog(props) {
    const [testLink, setTestLink] = useState(null)
    const {id, setOpen} = props
    const handleClose = () => {
        setOpen(false);
    };
    const createLink = () => {
        setTestLink(null)
        let host = `${window.location.origin}`
        let link = host + `/#${routes.testSubmit}/` + id
        setTestLink(link)
    }

    return (
        <div>
            <Dialog open={true} onClose={handleClose} aria-labelledby="form-dialog-title">
                <DialogTitle id="form-dialog-title">Ссылка на тест</DialogTitle>
                <DialogContent>
                    <DialogContentText>
                        Создайте ссылку и передайте её участникам тестирования, чтобы дать возможность пройти тест
                    </DialogContentText>
                    {testLink &&
                    <div>
                        <TextField
                            style={{width: 300}}
                            disabled={true}
                            value={testLink}
                            label="Ссылка на тест"/>
                        <IconButton onClick={() => navigator.clipboard.writeText(testLink)}>
                            <FileCopyIcon/>
                        </IconButton>
                    </div>
                    }
                </DialogContent>
                <DialogActions>
                    <Button onClick={handleClose} color="primary">
                        Отмена
                    </Button>
                    {!testLink &&
                    <Button onClick={createLink} color="primary">
                        Создать
                    </Button>}
                    {testLink &&
                    <Button onClick={handleClose} color="primary">
                        Готово
                    </Button>}
                </DialogActions>
            </Dialog>
        </div>
    );
}