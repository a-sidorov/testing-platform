import React from "react";
import {Grid, Paper, Typography} from "@material-ui/core";
import {getQuestionByType} from "../utils";

export function TestShow(props) {
    const {formData} = props
    return (
        <Grid
            container
            direction="column"
            justify="center"
            alignItems="center"
            style={{marginBottom: "40px", marginTop: "20px"}}
        >
            <Grid item xs={12} sm={5} style={{width: '100%'}}>
                <Grid style={{borderTop: '10px solid teal', borderRadius: 10}}>
                    <Paper elevation={2} style={{width: '100%'}}>
                        <div style={{
                            display: 'flex',
                            flexDirection: 'column',
                            alignItems: 'flex-start',
                            marginLeft: '15px',
                            paddingTop: '20px',
                            paddingBottom: '20px'
                        }}>
                            <Typography variant="h4"
                                        style={{fontFamily: 'sans-serif Roboto', marginBottom: "15px"}}>
                                {formData?.name}
                            </Typography>
                            <Typography variant="subtitle1">{formData?.description}</Typography>
                            <Typography
                                variant="subtitle1">{`${formData?.startDate} -> ${formData?.endDate}`}</Typography>
                        </div>
                    </Paper>
                </Grid>

                {formData?.questions?.map((question, i) => (
                        getQuestionByType(true, question, i)
                    )
                )}
            </Grid>
        </Grid>
    )
}