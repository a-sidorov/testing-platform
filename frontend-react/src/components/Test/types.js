/*
interface TestSubmitForm {
    id: number
    answers: {
        questionId: number,
        answer: number | number[] | string // oneOption, manyOptions, textAnswer
    }[]
}

interface Answer {
    id: number,
    content: string
}

interface QuestionProps {
    id: number,
    questionText: string,
    answers: Answer[],
    updateAnswerPickIds: (answerIds: number[]) => void
}

interface TextAnswerQuestion {
    id: number,
    question: string,
    updateAnswerText: (text: string) => void
}

export {Answer, QuestionProps, TextAnswerQuestion}*/
export const questionTypes = {
    manyOptions: 'MANY_OPTIONS',
    oneOption: 'ONE_OPTION',
    textAnswer: 'TEXT_ANSWER'
}