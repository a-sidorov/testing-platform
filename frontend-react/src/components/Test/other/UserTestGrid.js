import Grid from "@material-ui/core/Grid";
import Card from "@material-ui/core/Card";
import CardMedia from "@material-ui/core/CardMedia";
import CardContent from "@material-ui/core/CardContent";
import Typography from "@material-ui/core/Typography";
import CardActions from "@material-ui/core/CardActions";
import Button from "@material-ui/core/Button";
import Container from "@material-ui/core/Container";
import React from "react";
import {makeStyles} from "@material-ui/core/styles";
import {routes} from "../../utils";
import {useHistory} from "react-router-dom";

const useStyles = makeStyles((theme) => ({
    cardGrid: {
        paddingTop: theme.spacing(8),
        paddingBottom: theme.spacing(8),
    },
    card: {
        height: '100%',
        display: 'flex',
        flexDirection: 'column',
    },
    cardMedia: {
        paddingTop: '56.25%', // 16:9
    },
    cardContent: {
        flexGrow: 1,
    }
}));

export function UserTestGrid(props) {
    const history = useHistory();
    const classes = useStyles();
    const {tests} = props
    //todo: what if no tests? Show some 'empty'?

    const showTest = (test_id) => {
        console.log("show: " + test_id)
        history.push({
            pathname: routes.testView,
            search: `id=${test_id}&edit=false`
        })
    }

    const editTest = (test_id) => {
        history.push({
            pathname: routes.testView,
            search: `id=${test_id}&edit=true`
        })
    }

    return (
        <Container className={classes.cardGrid} maxWidth="md">
            <Grid container spacing={4}>
                {tests?.length === 0 &&
                <Typography gutterBottom variant="h5" component="h2" style={{marginLeft: "25%"}}>
                    Вы не создали ни одного теста :(
                </Typography>
                }
                {tests.map((test) => (
                    <Grid item key={test.id} xs={12} sm={6} md={4}>
                        <Card className={classes.card}>
                            <CardMedia
                                className={classes.cardMedia}
                                image="https://source.unsplash.com/random"
                                title="Image title"
                            />
                            <CardContent className={classes.cardContent}>
                                <Typography gutterBottom variant="h5" component="h2">
                                    {test.name}
                                </Typography>
                                <Typography>
                                    {test.description}
                                </Typography>
                            </CardContent>
                            <CardActions>
                                <Button size="small" color="primary" onClick={() => showTest(test.id)}>
                                    Посмотреть
                                </Button>
                                <Button size="small" color="primary" onClick={() => editTest(test.id)}>
                                    Редактировать
                                </Button>
                            </CardActions>
                        </Card>
                    </Grid>
                ))}
            </Grid>
        </Container>
    )
}