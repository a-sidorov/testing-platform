export const routes = {
    main: '/',
    signup: '/user/signup',
    signin: '/user/signin',
    profile: '/user/profile',
    userTestAnswers: '/user-test-answers',
    testView: '/test-view',
    testSubmit: '/test-submit',
}

export const localStorageKeys = {
    jwtToken: "jwt_token",
    rememberMe: "remember_me",
}

const UiState = {
    Ready: 0,
    Error: 1,
    Loading: 2
}

export {UiState}