import React from "react";
import {routes} from "../utils";
import UserIcon from "@material-ui/icons/AccountCircle";
import {Menu, MenuItem} from "@material-ui/core";
import {makeStyles} from "@material-ui/core/styles";
import {useHistory} from "react-router-dom";
import {removeJwt} from "../../lib/storage";

const useStyles = makeStyles((theme) => ({
    userIcon: {
        marginRight: theme.spacing(2),
        height: 40,
        width: 40
    },
}))

export function UserMenu(props) {
    const history = useHistory()
    const setAuthenticated = props.state.authenticated
    const classes = useStyles();

    const [anchorEl, setAnchorEl] = React.useState(null);

    const handleClick = (event) => {
        setAnchorEl(event.currentTarget);
    };

    const logout = () => {
        removeJwt()
        setAuthenticated(false)
        console.log("logged out")
    }

    const showProfile = () => {
        history.push(routes.profile)
    }

    const handleClose = async (action) => {
        if (action) {
            action()
        }
        setAnchorEl(null);
    };
    const close = () => {
        setAnchorEl(null)
    }

    return (
        <div>
            <UserIcon className={classes.userIcon} aria-controls="simple-menu" aria-haspopup="true"
                      onClick={handleClick}/>
            <Menu
                id="user-menu"
                anchorEl={anchorEl}
                keepMounted
                open={Boolean(anchorEl)}
                onClose={close}
            >
                <MenuItem onClick={() => handleClose(showProfile)}>Профиль</MenuItem>
                <MenuItem onClick={() => handleClose(logout)}>Выйти</MenuItem>
            </Menu>
        </div>
    );
}