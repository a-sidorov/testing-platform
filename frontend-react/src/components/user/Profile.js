import React, {createRef, useEffect, useState} from 'react';
import Avatar from '@material-ui/core/Avatar';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Grid from '@material-ui/core/Grid';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import Typography from '@material-ui/core/Typography';
import {makeStyles} from '@material-ui/core/styles';
import Container from '@material-ui/core/Container';
import {NavLink, useHistory} from "react-router-dom";
import {localStorageKeys, routes, UiState} from "../utils";
import axios from "axios";
import {api} from "../../lib/api";
import style from "../App/App.module.scss";
import {AppBar, CircularProgress} from "@material-ui/core";
import Toolbar from "@material-ui/core/Toolbar";
import IconButton from "@material-ui/core/IconButton";
import HomeIcon from "@material-ui/icons/Home";
import DoneIcon from "@material-ui/icons/Done";
import EditIcon from "@material-ui/icons/Edit";
import LinkIcon from "@material-ui/icons/Link";
import {getJwt} from "../../lib/storage";

const useStyles = makeStyles((theme) => ({
    toolbar: {
        minHeight: 70,
        textAlign: "center",
        alignItems: 'flex-start',
        paddingTop: theme.spacing(1),
    },
    paper: {
        marginTop: theme.spacing(8),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: theme.palette.secondary.main,
    },
    form: {
        width: '100%',
        marginTop: theme.spacing(3),
    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
}));

export default function Profile() {
    const classes = useStyles();
    const history = useHistory();
    const jwtToken = getJwt()
    const [profileData, setProfileData] = useState({
        firstName: "",
        secondName: "",
        email: "",
        password: "",
    })
    const [editedProfileData, setEditedProfileData] = useState(profileData)
    const [edit, setEdit] = useState(false)
    const [state, setState] = useState(UiState.Ready)
    const [error, setError] = useState(null)
    const formRef = createRef()

    useEffect(() => {
        getProfileData()
    }, [])

    useEffect(() => {
        setEditedProfileData(profileData)
    }, [profileData])

    const dispatchError = (error) => {
        if (error?.response?.data) {
            console.log(error?.response?.data)
        }
        setError(error.toString())//todo
        setState(UiState.Error)
        console.log(error)
    }

    const dispatchLoading = () => {
        setState(UiState.Loading)
    }

    const dispatchReady = () => {
        setState(UiState.Ready)
    }

    async function getProfileData() {
        try {
            dispatchLoading()
            let profileData = await axios.get(api.profileGet, {
                headers: {
                    'Authorization': 'Bearer ' + jwtToken,
                }
            }).then((r) => r.data)
            console.log(profileData)
            setProfileData({...profileData, password: "somepasswordUcan'tsee"})
            dispatchReady()
        } catch (e) {
            dispatchError(e)
        }
    }

    function onCancel() {
        setEditedProfileData(profileData)
        setEdit(false)
    }

    async function saveEdit() {
        if (!formRef.current.reportValidity()) {
            return
        }
        try {
            dispatchLoading()
            await axios.post(api.profileEdit, editedProfileData, {
                headers: {
                    'Authorization': 'Bearer ' + jwtToken,
                }
            })
            setProfileData(editedProfileData)
            setEdit(false)
            navigateMain()
            dispatchReady()
        } catch (e) {
            dispatchError(e)
        }
    }

    const navigateMain = () => {
        history.push(routes.main)
    }

    return (
        <Container component="main" maxWidth="xs">
            {state !== UiState.Ready &&
            <div className={style.modal}>
                {
                    state === UiState.Loading && <CircularProgress color={"secondary"}/>
                }
                {
                    state === UiState.Error && <div>
                        <div className={style.close} onClick={() => dispatchReady()}>
                        </div>
                        <div className={style.error}>
                            {error}
                        </div>
                    </div>
                }
            </div>
            }

            <div className={classes.paper}>
                <Avatar className={classes.avatar}>
                    <LockOutlinedIcon/>
                </Avatar>
                <Typography component="h1" variant="h5">
                    Профиль
                </Typography>
                <form className={classes.form} id={"signup-form"} ref={formRef}>
                    <Grid container spacing={2}>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                autoComplete="fname"
                                name="name"
                                variant="outlined"
                                defaultValue={profileData.firstName}
                                value={editedProfileData?.firstName}
                                onChange={(e) =>
                                    setEditedProfileData({...editedProfileData, firstName: e.target.value})
                                }
                                required
                                fullWidth
                                id="name"
                                label="Имя"
                                autoFocus
                                disabled={!edit}
                            />
                        </Grid>
                        <Grid item xs={12} sm={6}>
                            <TextField
                                variant="outlined"
                                defaultValue={profileData.secondName}
                                value={editedProfileData.secondName}
                                required
                                onChange={(e) =>
                                    setEditedProfileData({...editedProfileData, secondName: e.target.value})
                                }
                                fullWidth
                                id="last_name"
                                label="Фамилия"
                                name="last_name"
                                autoComplete="lname"
                                disabled={!edit}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                value={profileData.email}
                                required
                                fullWidth
                                id="email"
                                label="Логин"
                                name="email"
                                autoComplete="email"
                                disabled={true}
                            />
                        </Grid>
                        <Grid item xs={12}>
                            <TextField
                                variant="outlined"
                                value={profileData.password}
                                required
                                fullWidth
                                name="password"
                                label="Пароль"
                                type="password"
                                id="password"
                                autoComplete="current-password"
                                disabled={true}
                            />
                        </Grid>
                    </Grid>

                    {edit ? <Grid container justify="flex-end">
                            <Grid item style={{marginRight: "20px"}}>
                                <Button
                                    type="button"
                                    onClick={saveEdit}
                                    fullWidth
                                    variant="contained"
                                    color="primary"
                                    className={classes.submit}>
                                    Готово
                                </Button>
                            </Grid>
                            <Grid item>
                                <Button
                                    type="button"
                                    onClick={() => onCancel()}
                                    fullWidth
                                    variant="outlined"
                                    color="primary"
                                    className={classes.submit}>
                                    Отмена
                                </Button>
                            </Grid>
                        </Grid> :
                        <div>
                            <Button
                                type="button"
                                onClick={() => {
                                    setEdit(true)
                                }}
                                fullWidth
                                variant="contained"
                                color="primary"
                                className={classes.submit}>
                                Редактировать
                            </Button>
                            <Button
                                component={NavLink}
                                type={"button"}
                                fullWidth
                                to={routes.main}
                                variant="outlined"
                                color="primary">
                                На главную
                            </Button>
                        </div>
                    }
                </form>
            </div>
        </Container>
    );
}