package ru.nsu.testplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import ru.nsu.testplatform.entity.Test;
import ru.nsu.testplatform.entity.User;

import java.util.List;
import java.util.Optional;

public interface TestRepository extends JpaRepository<Test, Long> {
    Optional<Test> findByOwnerEmailAndId(String user, Long id);

    List<Test> findAllByOwner(User owner);


    @Query(value = "delete from tests as t" +
            "       where t.owner_id = (select users.id from users where email = :user)" +
            "and t.id = :id", nativeQuery = true)
    @Modifying
    void removeByOwnerEmailAndId(String user, Long id);
}