package ru.nsu.testplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.testplatform.entity.questions.Question;

public interface QuestionRepository extends JpaRepository<Question, Long> {


}
