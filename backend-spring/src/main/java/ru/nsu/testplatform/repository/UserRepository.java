package ru.nsu.testplatform.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.nsu.testplatform.entity.User;

import java.util.Optional;

public interface UserRepository extends JpaRepository<User, Long> {
    Optional<User> findByEmail(String email);
}
