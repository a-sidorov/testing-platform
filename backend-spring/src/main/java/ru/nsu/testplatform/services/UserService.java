package ru.nsu.testplatform.services;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;
import ru.nsu.testplatform.dto.ProfileDto;
import ru.nsu.testplatform.dto.ResultDto;
import ru.nsu.testplatform.entity.TestUser;
import ru.nsu.testplatform.entity.User;
import ru.nsu.testplatform.repository.TestUserRepository;
import ru.nsu.testplatform.repository.UserRepository;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;

@Service
@RequiredArgsConstructor
public class UserService {

    private static final String EXIST = "USER_NOT_EXIST";

    private final UserRepository userRepository;
    private final TestUserRepository testUserRepository;

    public User getUserByLogin(String login) throws NotFoundException {
        return userRepository.findByEmail(login)
                .orElseThrow(() -> new NotFoundException(EXIST));

    }


    public ProfileDto getUserProfile(String login) throws NotFoundException {
        var user = userRepository.findByEmail(login)
                .orElseThrow(() -> new NotFoundException(EXIST));

        return ProfileDto.toDto(user);
    }

    @Transactional
    public ProfileDto editUserProfile(String login, ProfileDto profileDto) throws NotFoundException {
        var user = userRepository.findByEmail(login)
                .orElseThrow(() -> new NotFoundException(EXIST));

        user.setFirstName(profileDto.getFirstName());
        user.setSecondName(profileDto.getSecondName());
        user = userRepository.saveAndFlush(user);

        return ProfileDto.toDto(user);
    }

    public List<ResultDto> getUserAnswers(String login) throws NotFoundException {
        var user = userRepository.findByEmail(login)
                .orElseThrow(() -> new NotFoundException(EXIST));

        var testsUser = testUserRepository.findAllByUser(user);
        List<ResultDto> resultDtos = new ArrayList<>();
        for (TestUser t : testsUser) {
            resultDtos.add(ResultDto.mapDto(t));
        }
        return resultDtos;
    }
}