package ru.nsu.testplatform.services;

import javassist.NotFoundException;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import ru.nsu.testplatform.entity.questions.Question;
import ru.nsu.testplatform.repository.QuestionRepository;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import java.util.List;

@Service
@AllArgsConstructor
public class QuestionService {
    private final QuestionRepository questionRepository;
    @PersistenceContext
    private final EntityManager em;

    @Transactional
    public void removeByTestId(Long id) {
        em.createNativeQuery("delete from questions as q where q.test_id = :id")
                .setParameter("id", id)
                .executeUpdate();
    }


    @Transactional
    public void saveAll(List<Question> questions) {
        questions.forEach(em::persist);
    }

    public Question getQuestionById(Long id) throws NotFoundException {
        return questionRepository.findById(id).orElseThrow(() -> new NotFoundException("QUESTION_NOT_EXIST"));
    }

}
