package ru.nsu.testplatform.services;


import com.nimbusds.oauth2.sdk.util.StringUtils;
import lombok.RequiredArgsConstructor;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import ru.nsu.testplatform.dto.ProfileDto;
import ru.nsu.testplatform.dto.RegisterDto;
import ru.nsu.testplatform.entity.User;
import ru.nsu.testplatform.repository.UserRepository;
import ru.nsu.testplatform.security.jwt.JwtProvider;

import javax.security.auth.message.AuthException;
import java.nio.charset.StandardCharsets;
import java.util.Base64;
import java.util.HashMap;
import java.util.Map;

@Service
@RequiredArgsConstructor
public class AuthService {

    private final UserRepository userRepository;


    private final PasswordEncoder passwordEncoder;

    private final JwtProvider jwtProvider;

    public ProfileDto createUser(RegisterDto userDto) {

        var user = RegisterDto.toUser(userDto);

        user.setPassword(passwordEncoder.encode(user.getPassword()));

        return ProfileDto.toDto(userRepository.save(user));
    }

    public String createAuthToken(String authorization) throws AuthException {
        authorization = cutAuthCode(authorization);
        Map<String, String> loginAndPasswordMap = getLoginAndPassword(authorization);
        String login = loginAndPasswordMap.get("login");
        String password = loginAndPasswordMap.get("password");

        var userEntity = userRepository.findByEmail(login)
                .orElseThrow(() -> new AuthException("USER_NOT_EXIST"));


        if (!checkPasswordUser(password, userEntity)) throw new AuthException("Неверный логин или пароль");

        return jwtProvider.generateToken(userEntity.getEmail());
    }

    private String cutAuthCode(String authorization) throws AuthException {
        if (StringUtils.isNotBlank(authorization) && authorization.startsWith("Basic")) {
            authorization = authorization.substring(6);
            if (StringUtils.isBlank(authorization)) {
                throw new AuthException("Пустые данные авторизации");
            }
        } else {
            throw new AuthException("Пустые данные авторизации");
        }
        return authorization;
    }

    private Map<String, String> getLoginAndPassword(String authHeader) throws BadCredentialsException {
        String customerCredentials = (convertFromBase64(authHeader));
        String login = customerCredentials.split(":")[0];
        String password = customerCredentials.split(":")[1];
        Map<String, String> loginAndPasswordMap = new HashMap<>();
        loginAndPasswordMap.put("login", login);
        loginAndPasswordMap.put("password", password);
        return loginAndPasswordMap;
    }

    private boolean checkPasswordUser(String password, User userEntity) {
        return passwordEncoder.matches(password, userEntity.getPassword());
    }

    private String convertFromBase64(String base64String) {
        byte[] bytes = base64String.getBytes(StandardCharsets.UTF_8);
        bytes = Base64.getDecoder().decode(bytes);
        return new String(bytes, StandardCharsets.UTF_8);
    }
}