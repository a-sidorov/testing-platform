package ru.nsu.testplatform.dto;


import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.extern.java.Log;
import lombok.extern.log4j.Log4j;
import lombok.extern.log4j.Log4j2;
import org.apache.logging.log4j.Level;
import ru.nsu.testplatform.entity.Test;

import java.util.ArrayList;
import java.util.List;

@Getter
@RequiredArgsConstructor
@EqualsAndHashCode
@Log4j2
public class TestDto {
    private final Long id;
    private final String name;
    private final String description;
    private final String startDate;
    private final String endDate;

    private final List<QuestionDto> questions;

    public static TestDto mapDto(Test test) {
        var questions = new ArrayList<QuestionDto>();
        for (var q : test.getQuestions()) {
            System.out.println("Question id is "+q.getId());
            questions.add(QuestionDto.mapDto(q));
        }

        log.log(Level.INFO,("Total dtos " + questions.size()));

        return new TestDto(
                test.getId(),
                test.getTestName(),
                test.getDescription(),
                test.getStartDate().toString(),
                test.getEndDate().toString(),
                questions);
    }

    public static TestDto mapDtoNoCorrect(Test test) {
        var questions = new ArrayList<QuestionDto>();
        for (var q : test.getQuestions()) {
            questions.add(QuestionDto.mapDtoNoCorrect(q));
        }
        return new TestDto(
                test.getId(),
                test.getTestName(),
                test.getDescription(),
                test.getStartDate().toString(),
                test.getEndDate().toString(),
                questions);
    }
}
