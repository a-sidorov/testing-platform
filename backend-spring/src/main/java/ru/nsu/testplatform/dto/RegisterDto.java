package ru.nsu.testplatform.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.testplatform.entity.User;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class RegisterDto {
    private String firstName;
    private String secondName;
    private String email;
    private String password;

    public static User toUser(RegisterDto registerDto) {
        return new User(
                registerDto.getFirstName(),
                registerDto.getSecondName(),
                registerDto.getEmail(),
                registerDto.getPassword(),
                null);
    }
}
