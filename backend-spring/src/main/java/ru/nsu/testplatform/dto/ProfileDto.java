package ru.nsu.testplatform.dto;

import lombok.*;
import ru.nsu.testplatform.entity.User;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class ProfileDto {
    private String firstName;
    private String secondName;
    private String email;

    public static ProfileDto toDto(User user) {
        return new ProfileDto(user.getFirstName(), user.getSecondName(), user.getEmail());
    }
}
