package ru.nsu.testplatform.dto;

import lombok.*;
import ru.nsu.testplatform.entity.TestUser;


@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
public class TestResultDto {
    private String email;
    private String result;

    public static TestResultDto mapDto(TestUser testUser){
        var res = new TestResultDto();
        res.setEmail(testUser.getUser().getEmail());
        res.setResult(testUser.getUserPoint() + "/" + testUser.getMaxPoint());

        return res;
    }
}
