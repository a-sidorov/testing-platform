package ru.nsu.testplatform.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.testplatform.entity.Test;

@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
public class ShortTestDto {
    private Long id;
    private String name;
    private String description;


    public static ShortTestDto mapDto(Test test) {
        return new ShortTestDto(test.getId(), test.getTestName(), test.getDescription());
    }
}
