package ru.nsu.testplatform.entity.questions;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import ru.nsu.testplatform.entity.identifiable.Identifiable;

import javax.persistence.*;

@Entity
@NoArgsConstructor
@AllArgsConstructor
@Getter
@Table(name = "question_options")
public class Option extends Identifiable {
    String optionText;
    boolean correct;

    @ManyToOne(cascade = CascadeType.PERSIST)
    @JoinColumn(name = "question_id", referencedColumnName = "id")
    Question question;
}
