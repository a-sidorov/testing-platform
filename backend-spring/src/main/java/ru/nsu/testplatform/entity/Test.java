package ru.nsu.testplatform.entity;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;
import ru.nsu.testplatform.entity.identifiable.Identifiable;
import ru.nsu.testplatform.entity.questions.Question;

import javax.persistence.*;
import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "tests")
public class Test extends Identifiable {


    @OneToMany(cascade = CascadeType.PERSIST,fetch = FetchType.EAGER)
    @JoinColumn(name = "test_id", referencedColumnName = "id")
    @OnDelete(action = OnDeleteAction.CASCADE)
    List<Question> questions = new ArrayList<>();

    @ManyToOne
    @JoinColumn(name = "owner_id", referencedColumnName = "id")
    private User owner;

    private String testName;
    private String description;

    private Date startDate;
    private Date endDate;
}
