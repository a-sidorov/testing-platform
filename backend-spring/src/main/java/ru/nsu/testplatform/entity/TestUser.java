package ru.nsu.testplatform.entity;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import ru.nsu.testplatform.entity.identifiable.Identifiable;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;
import javax.persistence.Table;


@Entity
@Setter
@Getter
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "passed")
public class TestUser extends Identifiable {

    @ManyToOne
    private Test test;

    @ManyToOne
    private User user;

    private Integer maxPoint;

    private Integer userPoint;
}
