package ru.nsu.testplatform.security;

import javassist.NotFoundException;
import lombok.RequiredArgsConstructor;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import ru.nsu.testplatform.entity.User;
import ru.nsu.testplatform.services.UserService;


@Service
@RequiredArgsConstructor
public class CustomUserDetailsService implements UserDetailsService {

    private final UserService userService;

    @Override
    public CustomUserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User userEntity = null;
        try {
            userEntity = userService.getUserByLogin(username);
        } catch (NotFoundException e) {
            throw new UsernameNotFoundException("USER_NOT_EXIST");
        }
        return CustomUserDetails.fromUserEntityToCustomUserDetails(userEntity);
    }
}